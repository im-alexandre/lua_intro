result = math.abs(-7)

-- math.ceil arredonda para cima
arredondado = math.ceil(3.1) -- 4
print(arredondado)

-- arredonda para baixo (truncamento)
truncado = math.floor(3.1)
print(truncado)

-- Calcula o resto de uma divisão
resto = math.fmod(5, 2)
print(resto)

-- Existem diversas funções avançadas no módulo math cerca de 27 funções
