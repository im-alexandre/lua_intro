#!/usr/bin/env lua

operations = require('packages/operacoes')
email = require('./packages/email')

command = arg[1]
a = arg[2]
b = arg[3]

if command == "sum" then
    print(operations.add(a, b))
elseif command == "subtract" then
    print(operations.subtract(a, b))
elseif command == "multiply" then
    print(operations.multiply(a, b))
elseif command == "divide" then
    print(operations.divide(a, b))
elseif command == "validate" then
    print(email.validade(a))
elseif command == "provider" then
    print(email.provider(a))
else
    error("Unknown command: " .. command)
end
