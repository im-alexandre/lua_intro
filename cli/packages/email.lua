email = {}

function is_valid(input)
    at = string.find(input, '@')
    if at == nil then
        return false
    end
    return input
end

function email.validade(input)
    if is_valid(input) then
        return input
    else
        return "Invalid email"
    end
end


function email.provider(input)
    if not is_valid(input) then
        return "invalid email"
    end
    at = string.find(input, '@')
    name = string.sub(input, at + 1, #input)
    return name
end


return email


