operations = {}
function operations.add(a, b)
    return a + b
end

function operations.divide(a, b)
    return a / b
end

function operations.multiply(a, b)
    return a * b
end

function operations.subtract(a, b)
    return a - b
end

return operations

