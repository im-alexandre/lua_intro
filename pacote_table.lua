list = {1, 2, 3}

table.insert(list, 2, "A")

for _, value in pairs(list) do
    print(value)
end

print("\nUtilizando o concat")
result = table.concat(list, ", ")
print(result)

print("\nLista ordenada")
list = {"x", "d", "b", "n"}
table.sort(list)
print(table.concat(list, ", "))

print("\nRemovendo item da lista")
list = { 1, 2, 3, 4, 5, 6 }
table.remove(list, 3)
print(table.concat(list, ", "))

print("\nFiltrando lista usando o remove")
list = { 1, 2, 3, 4, 5, 6 }
for index, value in pairs(list) do
    if value % 2 ~= 0 then
        table.remove(list, index)
    end
end
for _,v in pairs(list) do
    print(v)
end
