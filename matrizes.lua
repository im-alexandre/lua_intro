-- Matriz é uma lista/tabela de tabelas

matriz = {
    {'a', 'b', 'c'},
    {'d', 'e', 'f'},
    {'g', 'h', 'i'}
}

print(matriz[1][2])
print(matriz[2][3])

for _, line in pairs(matriz) do
    for _,value in pairs(line) do
        print(value)
    end
end
