function reduce(list, initial, fn)
    result = initial
    for _, value in pairs(list) do
        result = fn(result, value)
    end
    return result
end

function sum(a, b)
    return a + b
end

function concat(a, b)
    return a .. b
end

list = {2, 4, 6}
list_b = {"A", "B", "C", "D"}
print(reduce(list, 0, sum))
print(reduce(list_b, "", concat))

