function greaterThanTen(lista)
    local result = {}
    print("-----")
    print("Valores maiores que 10: ")
    for _, value in pairs(lista) do
        if value > 10 then
            print(value)
            table.insert(result, value)
        end
    end
    return result
end

function first(list)
    for _, value in pairs(list) do
        if result == nil then
            local result = value
            break
        end
    end
    return result
end

function last(list)
    for _, value in pairs(list) do
        local result = value
    end
    return result
end

function mean(lista)
    return sum(lista)/count(lista)
end

function sum(lista)
    local result = 0
    for _, value in pairs(lista) do
        result = result + value
    end
    return result
end

function count(lista)
    local result=0
    for _, value in pairs(lista) do
        result = result + 1
    end
    return result
end

lista = {10, 20, 50}

print(first(lista))
print("-----")
print(last(lista))
print("-----")

print(
    "-----\n" ..
    "Média: " .. mean(lista) .. "\n" ..
    "Soma: " .. sum(lista) .. "\n" ..
    "Contagem: " .. count(lista) .. "\n"
)

print("-----")
greaterThanTen(lista)
