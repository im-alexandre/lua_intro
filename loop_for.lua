-- Inclui o final
for number=1, 4 do
    print(number)
end

print("-----------")

-- O terceiro parâmetro indica o tamanho do "salto"
for number=2, 6, 2 do
    print(number)
end

print("-----------")

-- Sequência negativa
for i = 6, 0, -2 do
    print(i)
end