-- text = "Hello World! "
-- tamanho = string.len(text)
-- partial = string.sub(text, 1, 5)

-- print("Tamanho: " .. tamanho)
-- print("Tamanho com #text: " .. #text)
-- print("Sub: " .. partial)

-- print("Tudo maúsculo: " ..
--  string.upper(text))

-- print("Tudo minúsculo: " .. string.lower(text))
-- print("Repetido 2x: " .. string.rep(text, 2))

-- print("Texto invertido: " .. string.reverse(text))

emails = {
    "alexandre@gmail.com",
    "alexandre@hotmail.com"
}

for _, value in pairs(emails) do
    at = string.find(value, "gmail.com")
    if at then
        print("O email " .. value .. " é do gmail")
    else
        print("O email " .. value .. " não é do gmail.")
    end
end

-- string.find retorna dois valores - início e fim do padrão procurado
from, to = string.find(emails[1], "gmail")
print(from, to)
-- string.sub faz um slice do primeiro argumento, passando o início e fim do slice
print(string.sub(emails[1], from, to))

-- string.match retorna exatamente o que corresponde ao padrão procurado
result = string.match(emails[1], "gmail")
print(result) -- retorna "gmail"
result = string.match(emails[1], "yahoo")
print(result) -- retorna nil

-- procurando o provedor de email
at = string.find(emails[1], "@")
print(
    string.sub(emails[1], at+1)
)
-- outra forma
print(
    string.sub(emails[1], at+1, #emails[1])
)

function procuraEmail(email)
    at = string.find(email, "@")
    if at == nil then
        return "Email inválido!!!"
    end
    return string.sub(emails[1], at+1, #emails[1])
end
print("---- Usando uma função -----")
print(procuraEmail(emails[1]))
print(procuraEmail("alexandre.com.br"))

-- substituindo strings com gsub
text = "Pega uma caixa de bananas com duas bananas"
-- substitui todas as ocorrências
text1, valor = string.gsub(text, "bananas", "laranjas")
print("substitui todas as ocorrências")
print(text1)
-- substitui 1 ocorrência
print("substitui 1 ocorrência")
text2, valor = string.gsub(text, "bananas", "laranjas", 1)
print(text2)