-- Nulo
empty = nil

--Boolean
valid = false
notValid = true

--string
content = "hello world"
otoContent = 'hello world'

-- Listas (super importantes)
lista = {
    color = 'red',
    value = 10
}

-- Como acessar valores de uma lista
print(lista.color)
print(lista['value'])