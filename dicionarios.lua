-- Qualquer tipo de dados pode ser utilizado como chave
-- Porém, caso seja necessário especificar o tipo ou utilizar um tipo diferente,
-- deve-se utilizar o colchete
--
-- estruturas chave-valor não garantem a ordem das chaves e dos valores

key = "cor"
list = { 
    [key] = "vermelho",
    ["color"] = "red",
    [14] = "quatorze",
    [true] = "verdadeiro",
    c = 4
}

print(list)

for index, valor in pairs(list) do
    print(tostring(index) .. "> " .. valor)
end

return list
