list = {10, 98, 29, 47}

for position=1,4 do
    print(position .. ": " .. list[position])
end

for _, value in pairs(list) do
    print(value)
end

table.insert(list, 90)

for position, value in pairs(list) do
    print(position .. ": " .. value)
end

for position, value in ipairs(list) do
    print(position .. ": " .. value)
end
