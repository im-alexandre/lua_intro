function sum(a, b)
    return a+b
end

function capitalize(word)
    first = string.sub(word, 1, 1)
    remaining = string.sub(word, 2, #word)
    return (
        string.upper(first) ..
        string.lower(remaining)
        )
end

print(capitalize("bLUe"))
